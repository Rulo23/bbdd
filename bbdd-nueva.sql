source bbdd-antigua.sql

DROP TABLE dirige;

ALTER TABLE PERSONA ADD COLUMN edad INT;

ALTER TABLE ESTETICA ADD COLUMN estilo VARCHAR(15);

ALTER TABLE ESTETICA MODIFY color VARCHAR(20);

ALTER TABLE PERFUME ADD COLUMN marca VARCHAR(20);

ALTER TABLE EMPLEADO ADD COLUMN jefe_DNI_PERSONA CHAR(9);

ALTER TABLE EMPLEADO ADD COLUMN comision DOUBLE;

ALTER TABLE PROVEEDOR ADD COLUMN precio_provee DOUBLE;

ALTER TABLE PERSONA DROP COLUMN direccion;

ALTER TABLE PRODUCTO DROP COLUMN cantidad;

ALTER TABLE PERFUME DROP COLUMN intensidad;

ALTER TABLE TELEFONO MODIFY telefono_PERSONA INT(9);

ALTER TABLE PROVEEDOR MODIFY n_proveedor BIGINT;

ALTER TABLE CLIENTE MODIFY n_socio MEDIUMINT;

ALTER TABLE EMPLEADO MODIFY sueldo DOUBLE;

ALTER TABLE CLIENTE MODIFY n_socio BIGINT;

ALTER TABLE ESTANTERIA DROP PRIMARY KEY;

ALTER TABLE ESTANTERIA ADD PRIMARY KEY(seccion, n_estanteria);

ALTER TABLE EMPLEADO ADD CONSTRAINT FK_EMPLEADO3 FOREIGN KEY(jefe_DNI_PERSONA) REFERENCES EMPLEADO(DNI_PERSONA);





