source bbdd-nueva.sql

INSERT INTO PERSONA VALUES
('53678913P', 'Julian', 'Cabañero', 'Adobe', 'ju@gmail.com', '35'), 
('53625323T', 'Sergio', 'Sanz', 'Llamas', 'san4@gmail.com', '33'),
('53618913Q', 'Marta', 'Gomez', 'Ruiz', 'mar@gmail.com', '27'),
('53671913O', 'Adrian', 'Lamas', 'Azex', 'ad@gmail.com', '21'),
('43674513U', 'Montse', 'Faer', 'Beer', 'mont@gmail.com', '19'),
('53678913K', 'Jose', 'Mous', 'Smith', 'jose@gmail.com', '48'),
('53678913M', 'Consuelo', 'Gonzalez', 'Vite', 'consu@gmail.com', '27'),
('65893213L', 'Jesus', 'Fernandez', 'Zutra', 'chule@gmail.com', '66'),
('25986345P', 'Jesus', 'Sanchez', 'Homer', 'chus@gmail.com', '53'),
('25896316G', 'David', 'Alvarez', 'Bartolo', 'dav@gmail.com', '33'),
('32168764T', 'Matilde', 'Urdaneta', 'Camarillo', 'mati@gmail.com', '59'),
('02359874H', 'Nerea', 'Montero', 'Maben', 'nereamo@gmail.com', '25'),
('56526849S', 'Silvia', 'Espinosa', 'De Rivera', 'siles@gmail.com', '23'),
('98465135M', 'Laura', 'Bravo', 'Iglesias', 'lauri@gmail.com', '22'),
('02184658W', 'Maria', 'Peña', 'Gracia', 'marpe@gmail.com', '44');

INSERT INTO TELEFONO VALUES
('675412343', '53678913P'),
('723567132', '53625323T'),
('695412353', '53618913Q'),
('700345123', '53671913O'),
('701224124', '43674513U'),
('645629081', '53678913K'),
('703568621', '53678913M'),
('653983113', '65893213L'),
('678913537', '25986345P'),
('727983412', '25896316G'),
('725423395', '32168764T'),
('671312983', '02359874H'),
('714323423', '56526849S'),
('613421258', '98465135M'),
('643273846', '02184658W');

INSERT INTO ALMACEN(direccion) VALUES
('Pepito Perez'),
('Ratoncito Perez'),
('La Elipa'),
('Poeta Blas Otero'),
('Jose Luis Arrese');

INSERT INTO EMPLEADO(DNI_PERSONA, sueldo, n_empleado, n_producto_vende, jefe_DNI_PERSONA, comision) VALUES
('53625323T', '700.90', '28', '20', '53678913P', '23.4'), 
('53618913Q', '693.00', '34', '10', '53678913P', '12.7'),
('53671913O', '500.70', '12', '05', '53678913P', '21.13'), 
('43674513U', '1000.89', '20', '30', '65893213L', '13.23'), 
('53678913K', '2000.00', '10', '15', '65893213L', '24.12'),

INSERT INTO ESTANTERIA(n_estanteria, seccion) VALUES
('1', 's'),
('2', 'm'),
('3', 'l'),
('4', 'v'),
('5', 'b');

INSERT INTO PROVEEDOR VALUES
('53678913K', '205345', 'ferroviario','1000.00'),
('53678913M', '205984', 'marítimo', '1200.00'),
('65893213L', '205689', 'aéreo', '1400.00'),
('25986345P', '205148', 'carretera', '1600.00'),
('25896316G', '205106', 'aéreo', '1800.00');

INSERT INTO CLIENTE VALUES
('32168764T', '02302654', '20', '35'),
('02359874H', '02302645', '5', '45'),
('56526849S', '02302698', '10', '36'),
('98465135M', '02302648', '17', '12'),
('02184658W', '02302520', '6', '10');

INSERT INTO PEDIDO VALUES
('32168764T', '00659617', 'PayPal', '10.05.2017', '20.05.2017'),
('02359874H', '00659512', 'Visa', '05.06.2018', '10.06.2018'),
('56526849S', '00659610', 'MasterCard', '03.10.2018', '05.10.2018'),
('98465135M', '00658961', 'American Express', '06.11.2018', '20.11.2018'),
('02184658W', '00658205', 'PayPal', '20.12.2017', '30.12.2017');

INSERT INTO PRODUCTO VALUES
('105429', 'Inglot', 'Base maquillaje', 'Líquido', '25.95', '2019.09.08', '53625323T', '5', '00659617', '1', '53678913K', '17', '20'),
('105679', 'Wartheimer', 'Chanel', 'Líquido', '10.09', '2019.05.19', '53618913Q', '21', '00659512', '1', '53678913M', '80', '10'),
('106565', 'Normon', 'Paroxetina', 'Comprimido', '5.25', '2019.11.08', '53671913O', '10', '00659610', '24', '65893213L', '3', '30'),
('106520', 'Combix', 'Flutox', 'Líquido', '9.95', '2020.05.23', '43674513U', '30', '00658961', '1', '25986345P', '4', '05'),
('105487', 'Essence', 'Barra Labios', 'sólido', '2.99', '2020.08.21', '53678913K', '05', '00658205', '1', '25896316G', '1', '15');

INSERT INTO MEDICAMENTO VALUES
('105429', 'ocho horas', 'un gramo'), 
('105679', 'dos dias', '600 mililigramos'), 
('106565', 'dieciseis horas', '500 miligramos'), 
('106520', 'cuatro horas', '4 miligramos'), 
('105487', 'doce horas', '20 miligramos');

INSERT INTO ESTETICA VALUES
('105429', 'carne claro', 'cara', 'juvenil'),
('105679', 'rojo', 'uñas', 'vintage'),
('106565', 'verde', 'ojos', 'maduro'),
('106520', 'carne oscuro', 'cara', 'hipster'),
('105487', 'Morado', 'labios', 'streetwhere');

INSERT INTO PERFUME VALUES
('105429', 'fuerte', 'Vainilla', 'Hugo Boss'),
('105679', 'medio', 'Fresa', 'Lacoste'),
('106565', 'bajo', 'Mandarina', 'Channel'),
('106520', 'fuerte', 'Melón', 'Sfera'),
('105487', 'medio', 'Sandía', 'Hollister');



