CREATE TABLE Facturas(
CodigoCliente INTEGER, 
BaseImponible NUMERIC(15,2),
IVA NUMERIC(15,2),
Total NUMERIC(15,2),
CodigoFactura INTEGER,

FOREIGN KEY(CodigoCliente) REFERENCES Clientes(CodigoCliente));

CREATE TABLE Comisiones(
Comision NUMERIC(15,2),
CodigoFactura INTEGER,
CodigoEmpleado INTEGER(11),

FOREIGN KEY (CodigoFactura) REFERENCES Facturas(CodigoFactura), 
FOREIGN KEY (CodigoEmpleado) REFERENCES Empleados(CodigoEmpleado));

CREATE TABLE AsientoContable(
CodigoCliente INTEGER,
DEBE NUMERIC(15,2),
HABER NUMERIC(15,2),

FOREIGN KEY (CodigoCliente) REFERENCES Clientes(CodigoCliente));

CREATE TABLE ActualizacionLimiteCredito(
Fecha DATE,
CodigoCliente INTEGER,
Incremento INT,

FOREIGN KEY (CodigoCliente) REFERENCES Clientes(CodigoCliente));