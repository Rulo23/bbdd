DROP PROCEDURE IF EXISTS Facturar;

DELIMITER |

CREATE PROCEDURE Facturar()

BEGIN

  DECLARE NumPedidos INTEGER;
  DECLARE PedidoCaro INTEGER;
  DECLARE x INT;
  DECLARE y INT;
  DECLARE CodigoFactura INTEGER;
  DECLARE CodigoCliente INTEGER;
  DECLARE Factura NUMERIC(15,2);
  DECLARE BaseImponible NUMERIC(15,2);
  DECLARE ComEmpleado INTEGER; /*Comision del empleado*/
  DECLARE ComIncremento NUMERIC(15,2);
 
  SET NumPedidos = (SELECT COUNT(*) FROM Pedidos);           
  SET PedidoCaro = (select MAX(CodigoPedido) FROM Pedidos);  

  SET x = 1;

  while x <= PedidoCaro do

  SET y = 1;
  
  while y <= PedidoCaro do

   /* Codigo de las facturas y codigo del cliente de la tabla pedidos */

   SET CodigoFactura = (SELECT CodigoPedido FROM Pedidos WHERE CodigoCliente = x AND CodigoPedido = y); 
                                    
   SET CodigoCliente = (SELECT CodigoCliente FROM Pedidos WHERE CodigoCliente = x AND CodigoPedido = y); 
/*----------------------------------------------------------------------*/  
                    
   SET Factura = (SELECT SUM(Cantidad * PrecioUnidad) AS 'Precio Total Pedido' DetallePedidos WHERE CodigoPedido = y);    
  
   SET BaseImponible = (SELECT SUM((Cantidad * PrecioUnidad) % 1.21) AS 'Precio sin IVA' FROM DetallePedidos WHERE CodigoPedido = y);
  
/*----------------------------------------------------------------------*/ 
        /*Metemos datos en la tabla facturas y en la tabla Comisiones*/
	
	IF Factura IS NOT NULL then

            IF CodigoCliente IS NOT NULL then 

             INSERT INTO Facturas (CodigoCliente, BaseImponible, Total, CodigoFactura) VALUES (
	     CodigoCliente, BaseImponible, NULL, Factura, CodigoFactura);

             SET ComEmpleado = (SELECT CodigoEmpleadoRepVentas FROM Clientes WHERE CodigoCliente = CodigoClientePedido);

             SET ComIncremento = (SELECT SUM((Cantidad * PrecioUnidad) * 0.05) FROM DetallePedidos WHERE CodigoPedido = y);

              IF ComIncremento IS NOT NULL then

                INSERT INTO Comisiones VALUES (ComIncremento, CodigoFactura, ComEmpleado);
              
	      end IF;

             end IF;

         end IF;

     SET y = y + 1;

    end while;

    SET x = x + 1;

  end while;

END;

|

DELIMITER ;
