/*Procedimiento GenerarContabilidad que guardará en la tabla AsientoContable un registro por cada grupo de facturas, almacenado en el campo DEBE la suma de las facturas que han sido generadas.*/

DROP PROCEDURE IF EXISTS GeneracionContabilidad;

DELIMITER |

CREATE PROCEDURE GeneracionContabilidad()

BEGIN

  DECLARE x INT;
  DECLARE NumClientes INTEGER;
  DECLARE CodigoCliente INTEGER;
  DECLARE GastoTotalCliente NUMERIC(15,2);

  SET x = 0;

  SET NumClientes = (SELECT COUNT(*) FROM Clientes);

  while x <= NumClientes do
   
    SET CodigoCliente = (SELECT CodigoCliente.Clientes FROM Clientes WHERE CodigoCliente.Clientes = x);

/*----------------------------------------------------------------------*/
   
  /* Hacemos la suma del total gastado en los pedidos de cada cliente y despues metemos los datos en la tabla*/

      IF CodigoCliente IS NOT NULL then
     
        SET GastoTotalCliente = (SELECT SUM(Total) FROM Facturas WHERE CodigoCliente = x GROUP BY CodigoCliente);

       IF GastoTotalCliente IS NOT NULL then
        
      	 INSERT INTO AsientoContable VALUES (CodigoCliente, GastoTotalCliente, GastoTotalCliente);
      
       end IF;

      end IF;

    SET x = x + 1;
  
  end while;

END;

|

DELIMITER ;
