DROP USER IF EXISTS 'proveedor'@'localhost';
DROP USER IF EXISTS 'jefe'@'localhost';
DROP USER IF EXISTS 'empleado'@'localhost';
DROP USER IF EXISTS 'cliente'@'localhost';


CREATE USER 'proveedor'@'localhost' IDENTIFIED BY 'proveedor';
CREATE USER 'jefe'@'localhost' IDENTIFIED BY 'boss';
CREATE USER 'empleado'@'localhost' IDENTIFIED BY 'empleado';
CREATE USER 'cliente'@'localhost' IDENTIFIED BY 'cliente';



GRANT SELECT(fecha_emision, fecha_entrega) ON farmacia.PEDIDO TO 'proveedor'@'localhost';
GRANT ALL PRIVILEGES ON farmacia.* TO 'jefe'@'localhost';
GRANT SELECT, UPDATE, DELETE ON farmacia.PRODUCTO TO 'empleado'@'localhost';
GRANT SELECT, UPDATE on farmacia.PRODUCTO TO 'cliente'@'localhost';







